#/bin/sh

#===============================================================================#
#Color Defines
#===============================================================================#
UNDER='\e[4m'
RED='\e[31;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
BLUE='\e[34;1m'
MAGENTA='\e[35;1m'
CYAN='\e[36;1m'
WHITE='\e[37;1m'
ENDCOLOR='\e[0m'

#===============================================================================#
#Variables and Config Parameters
#===============================================================================#
ROOT_FOLDER="/local/mnt/workspace/snpe/"
SNPE_SDK="snpe-1.41.0.zip"
SNPE_SDK_INSTALLED="snpe-sdk"
PATH_GET_PIP="https://bootstrap.pypa.io/pip/3.5/get-pip.py"
LINK_ANDROID_NDK="https://dl.google.com/android/repository/android-ndk-r21e-linux-x86_64.zip"
FILE_ANDROID_NDK="android-ndk-r21e-linux-x86_64.zip"
LINK_ANDROID_TOOLS="https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip"
FILE_ANDROID_TOOLS="commandlinetools-linux-6858069_latest.zip"
PATH_ANDROID_TOOLS="cmdline-tools/bin"
PATH_ANDROID_NDK="android-ndk"
PATH_ANDROID_SDK="/usr/lib/android-sdk"
VITUAL_ENV="tf_env"
OUTPUT_FOLDER="output"

check_for_confirmation()
{
	msg=$@
	while true; do
		echo $msg
		read -r -p "Enter (Yy/Nn): " answer
		case $answer in
			[Yy]* ) break;;
			[Nn]* ) continue;;
			* ) echo "Please Answer Y to continue";;
		esac
	done
}
# Instructions to Begin with
start_instructions()
{
	isprint=$1 #Get the Print Command

	if [ -z "$isprint" ]
	then
		msg="Read Confirmation Required"
		check_for_confirmation $msg
	else
		echo -e $GREEN"This Script is used for demostrating the instructions\n"\
			"All Instructions used in the scripts are displayed in screen for future reference\n"\
			"Therefore the script is not mandatory\n"\
			$ENDCOLOR
	fi
}

# Install Tools- Python and Virtual Env
install_tools()
{
	isprint=$1 #Get the Print Command

	$isprint \
	sudo apt update
	$isprint \
	sudo apt install python3-pip android-sdk virtualenv openjdk-8-jdk zip
}

#Setup Root
setup_root()
{
	isprint=$1 #Get the Print Command

	$isprint \
	export ML_ROOT=$ROOT_FOLDER
	export ML_ROOT=$ROOT_FOLDER

	$isprint \
	mkdir -p $ML_ROOT/

	$isprint \
	mkdir -p $ML_ROOT/$OUTPUT_FOLDER/

	$isprint \
	cd $ML_ROOT

	if [ -z "$isprint" ]
	then
		msg="Copy Confirmation Required "$SNPE_SDK" to "$ROOT_FOLDER/$SNPE_SDK
		check_for_confirmation $msg
	else
		echo -e $GREEN"MANDATORY! Manual Step \n"\
			"Copy the SNPE SDK ("$SNPE_SDK") to "$ROOT_FOLDER/$SNPE_SDK \
			"before proceeding to next steps"$ENDCOLOR
	fi

	$isprint \
	ls -al $ML_ROOT
}

#Setup Android NDK
setup_android_ndk()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $ML_ROOT

	$isprint \
	export ANDROID_NDK_ROOT=$ML_ROOT/$PATH_ANDROID_NDK/
	export ANDROID_NDK_ROOT=$ML_ROOT/$PATH_ANDROID_NDK/

	$isprint \
	export ANDROID_HOME=$PATH_ANDROID_SDK/
	export ANDROID_HOME=$PATH_ANDROID_SDK/

	$isprint \
	SDK_MGR_ROOT=$ML_ROOT/$PATH_ANDROID_TOOLS
	SDK_MGR_ROOT=$ML_ROOT/$PATH_ANDROID_TOOLS

	$isprint \
	wget $LINK_ANDROID_NDK

	$isprint \
	unzip -q $FILE_ANDROID_NDK -d $ANDROID_NDK_ROOT

	$isprint \
	mv $ANDROID_NDK_ROOT/$PATH_ANDROID_NDK*/* $ANDROID_NDK_ROOT/

	$isprint \
	rm -rf $ANDROID_NDK_ROOT/$PATH_ANDROID_NDK*

	$isprint \
	wget $LINK_ANDROID_TOOLS

	$isprint \
	unzip -q $FILE_ANDROID_TOOLS

	$isprint \
	sudo chmod 777 -R $PATH_ANDROID_SDK

	$isprint \
	cd $SDK_MGR_ROOT

	$isprint \
	./sdkmanager --sdk_root=$PATH_ANDROID_SDK --licenses

	$isprint \
	ls -al $ML_ROOT $ANDROID_NDK_ROOT $PATH_ANDROID_SDK

}

#Setup Python Enviorment
setup_python_environment()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $ML_ROOT

	$isprint \
	virtualenv --python=python3 $ML_ROOT/$VITUAL_ENV

	$isprint \
	source $VITUAL_ENV/bin/activate

	$isprint \
	wget $PATH_GET_PIP

	$isprint \
	python3 get-pip.py

	$isprint \
	pip3 --version # Checking Pip3 is working

	if [ -z "$isprint" ]
	then
		msg="Confirm Successfull Installation of pip3, Otherwise quit and start again"
		check_for_confirmation $msg
	fi

	$isprint \
	pip install --upgrade pip

	$isprint \
	pip install --upgrade setuptools

	if [ -z "$isprint" ]
	then
		msg="Confirm Successfull Installation of Setup tools, Otherwise quit and start again"
		check_for_confirmation $msg
	fi

	$isprint \
	ls -al $ML_ROOT
}

#Setup SNPE SDK
setup_snpe_sdk()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $ML_ROOT

	$isprint \
	export SNPE_ROOT=$ML_ROOT/$SNPE_SDK_INSTALLED
	export SNPE_ROOT=$ML_ROOT/$SNPE_SDK_INSTALLED

	$isprint \
	unzip -q $SNPE_SDK -d $SNPE_ROOT
	
	$isprint \
	mv $SNPE_ROOT/snpe*/* $SNPE_ROOT/

	$isprint \
	rm -rf $SNPE_ROOT/snpe*

	$isprint \
	cd $SNPE_ROOT

	$isprint \
	source bin/dependencies.sh

	if [ -z "$isprint" ]
	then
		msg="All dependencies should be met before continue"
		check_for_confirmation $msg
	fi

	$isprint \
	pip install numpy==1.16.5 sphinx==2.2.1 scipy==1.3.1 matplotlib==3.0.3 scikit-image==0.15.0 protobuf==3.6.0 PyYAML==5.1 Mako

	$isprint \
	source bin/check_python_depends.sh
	if [ -z "$isprint" ]
	then
		msg="All dependencies should be met before continue"
		check_for_confirmation $msg
	fi

	$isprint \
	pip install tensorflow==1.11

	$isprint \
	pip install tensorflow==1.11

	if [ -z "$isprint" ]
	then
		echo -e $GREEN"Tensorflow version"$ENDCOLOR
		python -c "import tensorflow as tf; print(tf.__version__)"
	else
		echo "python -c \"import tensorflow as tf; print(tf.__version__)\""
	fi

	if [ -z "$isprint" ]
	then
		msg="Tensor Flow must be installed before you proceed, Confirm Instalation is successfull before continue"
		check_for_confirmation $msg
	fi

	$isprint \
	export TENSORFLOW_DIR=$VIRTUAL_ENV/lib/python3.5/site-packages/tensorflow/
	export TENSORFLOW_DIR=$VIRTUAL_ENV/lib/python3.5/site-packages/tensorflow/

	$isprint \
	source $SNPE_ROOT/bin/envsetup.sh -t $TENSORFLOW_DIR

	$isprint \
	ls -al $ML_ROOT $SNPE_ROOT
}

#Setup Inception V3
setup_inception_v3()
{
	isprint=$1 #Get the Print Command

	$isprint \
	INCEPTION_ROOT=$SNPE_ROOT/models/inception_v3
	INCEPTION_ROOT=$SNPE_ROOT/models/inception_v3

	$isprint \
	cd $SNPE_ROOT

	$isprint \
	mkdir -p $INCEPTION_ROOT/assets

	$isprint \
	mkdir -p $INCEPTION_ROOT/output

	$isprint \
	python models/inception_v3/scripts/setup_inceptionv3.py -d -a $INCEPTION_ROOT/assets

	$isprint \
	snpe-net-run --container $INCEPTION_ROOT/dlc/inception_v3.dlc \
		--input_list $INCEPTION_ROOT/data/cropped/raw_list.txt \
		--output_dir $INCEPTION_ROOT/output/

	$isprint \
	python  $INCEPTION_ROOT/scripts/show_inceptionv3_classifications.py \
		-i  $INCEPTION_ROOT/data/cropped/raw_list.txt \
		-o  $INCEPTION_ROOT/output/ \
		-l  $INCEPTION_ROOT/data/imagenet_slim_labels.txt

	if [ -z "$isprint" ]
	then
		msg="Confirm the inference results is successfull before continue"
		check_for_confirmation $msg
	fi

	$isprint \
	ls -al $ML_ROOT $SNPE_ROOT $INCEPTION_ROOT
}

#Setup Android Application
setup_android_classifiers()
{
	isprint=$1 #Get the Print Command

	$isprint \
	IMAGE_CLASSIFIERS_ROOT=$SNPE_ROOT/examples/android/image-classifiers
	IMAGE_CLASSIFIERS_ROOT=$SNPE_ROOT/examples/android/image-classifiers

	$isprint \
	cd $SNPE_ROOT

	$isprint \
	cp $SNPE_ROOT/android/snpe-release.aar $IMAGE_CLASSIFIERS_ROOT/app/libs/snpe-release.aar

	$isprint \
	cd $IMAGE_CLASSIFIERS_ROOT

	$isprint \
	bash ./setup_inceptionv3.sh

	$isprint \
	./gradlew assembleDebug

	$isprint \
	ls -al  $ML_ROOT $SNPE_ROOT $IMAGE_CLASSIFIERS_ROOT
}


#Copy Android Application
copy_android_apk()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $ML_ROOT

	$isprint \
	cp $IMAGE_CLASSIFIERS_ROOT/app/build/outputs/apk/app-debug.apk $ML_ROOT/$OUTPUT_FOLDER

	$isprint \
	ls -al $ML_ROOT $ML_ROOT/$OUTPUT_FOLDER
}
#===============================================================================#
#Utility Functions
#===============================================================================#
color()
{
	echo -e $1
}
print_line()
{
	echo -e $RED"========================================================================================================================" $ENDCOLOR
}
print_limits()
{
	intent=$1
	msg=$2
	print_line
	echo -e $YELLOW $@
	print_line
}

wait_for_user()
{
	msg=$@
	color $MAGENTA $UNDER
	echo $msg
	read -p "Enter to Continue"
	color $ENDCOLOR
}

exectute_cmd()
{
	call_fn=$1
	display_msg=$2
	print_limits $display_msg Commands
	color $CYAN
	$call_fn "echo"
	print_line
	wait_for_user "Please verify the instructions and proceed with execution"
	print_limits $display_msg Begin
	$call_fn ""
	print_limits $display_msg End
}

#===============================================================================#
#Main Routine
#===============================================================================#
exectute_cmd start_instructions "Instructions"
exectute_cmd install_tools "Setting Up the Tools Required"
exectute_cmd setup_root "Setting Up the Root Folder Stucture"
exectute_cmd setup_android_ndk "Setting Up Android NDK and SDK"
exectute_cmd setup_python_environment "Setting Up Python Virtual Enviorment"
exectute_cmd setup_snpe_sdk "Setting Up SNPE SDK"
exectute_cmd setup_inception_v3 "Setting Up Inception v3 model conversion"
exectute_cmd setup_android_classifiers "Setting Up Android Application"
exectute_cmd copy_android_apk "Copying Android Application"