@echo off
SET /A ARGS_COUNT=0
SET adbcommand=adb
TITLE [LOAD] (%cd%\)
FOR %%A in (%*) DO SET /A ARGS_COUNT+=1
if %ARGS_COUNT% == 1 SET adbcommand=%adbcommand% -s %1
echo %adbcommand%
@echo on
%adbcommand% reboot bootloader
pause
fastboot flash boot %cd%\mdm9607-perf-boot.img
fastboot flash system %cd%\mdm9607-perf-sysfs.ubi
fastboot flash usr_data %cd%\usrdata.ubi
pause
fastboot reboot

