#!/bin/bash
RED='\e[31;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
BLUE='\e[34;1m'
MAGENTA='\e[35;1m'
CYAN='\e[36;1m'
WHITE='\e[37;1m'
ENDCOLOR='\e[0m'
WORKDIR=`pwd`
BUILDNAME="EC25_SDK"
BUILDROOT="$WORKDIR/../$BUILDNAME"
PATCH_DIR="$WORKDIR/patches"
PATCH_DIR2="$WORKDIR/patches2"
PATCH_DIR3="$WORKDIR/patches3"
QUECTEL_SDK_FILE="EC25_sdk.tar.bz2"
SDK_PATH="ql-ol-sdk"
SDKROOT="$BUILDROOT/$SDK_PATH"
CAF_PATCHES_CLONE_LINK="https://github.qualcomm.com/Innovationlab/EC25.git -b caf_mirror"
PATCH_DIR_CAF="$WORKDIR/patches_caf"
QDN_PATCH_FILENAME="QDN_Relv1.0.zip"

# Setup SDK Folder
setup_folder()
{
    echo Begin
    if [ -e $BUILDROOT ]
    then
	    cd $BUILDROOT
    else 
	    mkdir $BUILDROOT
	    cd $BUILDROOT
    fi
}

# Extract SDK
setup_sdk()
{
  cd $PATCH_DIR
  echo Extract SDK Files to $BUILDROOT
  tar -xf $QUECTEL_SDK_FILE --checkpoint=10000 -C $BUILDROOT
}

# Function to download CAF patches
download_caf_mirror_patches()
{
  echo "Dowloading CAF patches ..."

  if [ -e $PATCH_DIR_CAF ]
  then
    rm -rf $PATCH_DIR_CAF
  else
    mkdir $PATCH_DIR_CAF
  fi
  git clone $CAF_PATCHES_CLONE_LINK $PATCH_DIR_CAF
}

# Function to apply CAF patches to CAF code
apply_caf_mirror_patches()
{
  download_caf_mirror_patches
  echo "Applying CAF patches ..."
  if [ ! -e $PATCH_DIR_CAF ]
  then
    echo -e "$RED $PATCH_DIR_CAF : Not Found $ENDCOLOR"
  exit
  fi
  cp -rf $PATCH_DIR_CAF/caf_mirror/./ $SDKROOT/
}

# Initialize Toolchain & configure kernel_menuconfig
initialize_toolchain()
{
  cd $SDKROOT
  echo Initializing SDK at $SDKROOT
  . ql-ol-crosstool/ql-ol-crosstool-env-init
}

# Function to initialize git
initialize_git()
{
  cd $SDKROOT
  git init .
  echo Adding All files to Git
  git add .
  echo Initial Commit
  git commit -q -m "Initial Commit"
  git repack -d -l
}

#Function to extract QDN patches
function extract_qdn_patches()
{
	cd $WORKDIR
	unzip $QDN_PATCH_FILENAME -d temp_folder
	cp -rf temp_folder/*/patches3 $PATCH_DIR3
	rm -rf temp_folder
}

# Function to apply patches
apply_ec25_patches()
{
	PATCH_DIR=$1
  echo Applying patches from $PATCH_DIR
	cd $PATCH_DIR
	patch_list=$(find . -type f -name "*.patch" | sort) &&
  cd $SDKROOT
	for patch in $patch_list; do
		echo $PATCH_DIR/$patch
		git am $PATCH_DIR/$patch
	done
}

# Build
build()
{
  cd $SDKROOT
  echo Starting Building, Might take few minutes to complete
  make kernel_menuconfig
  make -j16
}

echo -e "$BLUE EC25 Build Process Begins Here, Might take few hours to complete Wait Patiently $ENDCOLOR"

# Setup SDK Folder
setup_folder
# Extract SDK
setup_sdk
# Initialize Toolchain & configure kernel_menuconfig
initialize_toolchain
#3 Apply Patches for CAF Mirror
apply_caf_mirror_patches
# Function to initialize git
initialize_git
# Function to apply patches
apply_ec25_patches $PATCH_DIR2
# Function to apply patches
extract_qdn_patches
apply_ec25_patches $PATCH_DIR3
# Build
build

echo -e "$GREEN ### Make Completed Succesfully ### $ENDCOLOR"

echo End
