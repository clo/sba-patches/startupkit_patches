#/bin/bash

#===============================================================================#
#Variables and Config Parameters
#===============================================================================#
_config()
{
	WORKDIR=`pwd`
	BUILDNAME="RG500Q_SDK"
	MAKE_JOB_THREADS=4
	ENABLE_PROMTS=1
	PRINT_ONLY=0
	NO_TOOL_CHAIN_UPDATE=0
	GIT_USER_NAME="dev"
	GIT_USER_EMAIL="dev@$BUILDNAME"
	SDK_FILE="ql-ol-extsdk.tar.gz"
	SDK_DIR="ql-ol-extsdk"
	TOOLCHAIN_FILE="RG500QEAAAR11A01M4G_OCPU_01.001V02.01.001V02-toolchain.tar.gz"
	TOOLCHAIN_INSTALL_PATH="/opt/ql_crosstools"
	ROOTFS_FILE="ql-ol-rootfs.tar.gz"
	ROOTFS_DIR_NAME="ql-ol-rootfs"
	OUTPUT_DIR_NAME="target"
	SAMPLE_APPS_DIR_NAME="sample"
}

_reconfig()
{
	BUILDROOT="$WORKDIR/../$BUILDNAME"
	SDK_ROOT="$BUILDROOT/$SDK_DIR"
	ROOTFS_DIR="$SDK_ROOT/$ROOTFS_DIR_NAME"
	OUTPUT_DIR="$SDK_ROOT/$OUTPUT_DIR_NAME"
	SAMPLE_APPS_DIR="$SDK_ROOT/$SAMPLE_APPS_DIR_NAME"
	SAMPLE_BIN_DIR="$OUTPUT_DIR/$SAMPLE_APPS_DIR_NAME"
	PATCH_DIR1="$WORKDIR/patches"
	PATCH_DIR2="$WORKDIR/patches2"
	PATCH_DIR3="$WORKDIR/patches3"
	PATCH_DIR_OS="$WORKDIR/patches_os"
}

#===============================================================================#
# Display all Config Parameters
#===============================================================================#
print_config()
{
	echo "WORKDIR                 = "$WORKDIR
	echo "BUILDNAME               = "$BUILDNAME
	echo "MAKE_JOB_THREADS        = "$MAKE_JOB_THREADS
	echo "ENABLE_PROMTS           = "$ENABLE_PROMTS
	echo "PRINT_ONLY              = "$PRINT_ONLY
	echo "NO_TOOL_CHAIN_UPDATE    = "$NO_TOOL_CHAIN_UPDATE
	echo "GIT_USER_NAME           = "$GIT_USER_NAME
	echo "GIT_USER_EMAIL          = "$GIT_USER_EMAIL
	echo "BUILDROOT               = "$BUILDROOT
	echo "SDK_FILE                = "$SDK_FILE
	echo "SDK_DIR                 = "$SDK_DIR
	echo "SDK_ROOT                = "$SDK_ROOT
	echo "TOOLCHAIN_FILE          = "$TOOLCHAIN_FILE
	echo "TOOLCHAIN_INSTALL_PATH  = "$TOOLCHAIN_INSTALL_PATH
	echo "ROOTFS_FILE             = "$ROOTFS_FILE
	echo "ROOTFS_DIR_NAME         = "$ROOTFS_DIR_NAME
	echo "ROOTFS_DIR              = "$ROOTFS_DIR
	echo "OUTPUT_DIR_NAME         = "$OUTPUT_DIR_NAME
	echo "OUTPUT_DIR              = "$OUTPUT_DIR
	echo "SAMPLE_APPS_DIR_NAME    = "$SAMPLE_APPS_DIR_NAME
	echo "SAMPLE_APPS_DIR         = "$SAMPLE_APPS_DIR
	echo "PATCH_DIR1              = "$PATCH_DIR1
	echo "PATCH_DIR2              = "$PATCH_DIR2
	echo "PATCH_DIR3              = "$PATCH_DIR3
	echo "PATCH_DIR_OS            = "$PATCH_DIR_OS
}

check_for_confirmation()
{
	msg=$@
	if [ "$ENABLE_PROMTS" = "1" ]
	then
		while true; do
			echo $msg
			read -r -p "Enter (Yy/Nn): " answer
			case $answer in
				[Yy]* ) break;;
				[Nn]* ) continue;;
				* ) echo "Please Answer Y to continue";;
			esac
		done
	fi
}
# Instructions to Begin with
start_instructions()
{
	isprint=$1 #Get the Print Command

	if [ -z "$isprint" ]
	then
		msg="Read Confirmation Required"
		check_for_confirmation $msg
	else
		echo -e $GREEN"This Script is used for demostrating the instructions\n"\
			"All Instructions used in the scripts are displayed in screen for future reference\n"\
			"Therefore the script is not mandatory\n"\
			$ENDCOLOR
	fi
}

# Install Tools- Python and Virtual Env
install_tools()
{
	isprint=$1 #Get the Print Command

	$isprint \
	sudo apt update
	$isprint \
	sudo apt install zip
}

#Setup Root
setup_root()
{
	isprint=$1 #Get the Print Command

	$isprint \
	mkdir -p $PATCH_DIR1

	$isprint \
	rm -rf $BUILDROOT

	$isprint \
	mkdir -p $BUILDROOT

	$isprint \
	cd $BUILDROOT

	if [ -z "$isprint" ]
	then
		msg="Copy Confirmation Required "$SDK_FILE" to "$PATCH_DIR1/$SDK_FILE
		check_for_confirmation $msg
	else
		echo -e $RED"MANDATORY! Manual Step \n"$GREEN\
			"Copy the SNPE SDK ("$SDK_FILE") to "$PATCH_DIR1/$SDK_FILE \
			"before proceeding to next steps"$ENDCOLOR
	fi

	if [ -z "$isprint" ]
	then
		msg="Copy Confirmation Required "$TOOLCHAIN_FILE" to "$PATCH_DIR1/$TOOLCHAIN_FILE
		check_for_confirmation $msg
	else
		echo -e $RED"MANDATORY! Manual Step \n"$GREEN\
			"Copy the SNPE SDK ("$TOOLCHAIN_FILE") to "$PATCH_DIR1/$TOOLCHAIN_FILE \
			"before proceeding to next steps"$ENDCOLOR
	fi

	$isprint \
	ls -al $WORKDIR $PATCH_DIR1 $BUILDROOT
}

#Setup Tool Chain
setup_tool_chain()
{
	isprint=$1 #Get the Print Command

	if [ "$NO_TOOL_CHAIN_UPDATE" = "1" ]
	then
		return
	fi

	$isprint \
	cd $BUILDROOT

	$isprint \
	sudo rm -rf $TOOLCHAIN_INSTALL_PATH

	$isprint \
	sudo mkdir -p $TOOLCHAIN_INSTALL_PATH

	$isprint \
	sudo chmod 777 $TOOLCHAIN_INSTALL_PATH

	$isprint \
	tar -xf $PATCH_DIR1/$TOOLCHAIN_FILE -C $TOOLCHAIN_INSTALL_PATH

	$isprint \
	ls -al $TOOLCHAIN_INSTALL_PATH $TOOLCHAIN_INSTALL_PATH/*

}

#Setup SDK
setup_sdk()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $BUILDROOT

	$isprint \
	tar -xf $PATCH_DIR1/$SDK_FILE

	$isprint \
	cd $SDK_ROOT

	$isprint \
	mkdir -p $ROOTFS_DIR

	$isprint \
	sudo tar -xzf $ROOTFS_FILE -C $ROOTFS_DIR

	$isprint \
	sudo chown -R $USER $ROOTFS_DIR

	$isprint \
	ls -al $BUILDROOT $SDK_ROOT $SDK_ROOT/*

}

display_git_status()
{
	isprint=$1 #Get the Print Command

	$isprint \
	git log --oneline

	$isprint \
	git status
}


#Setup git
setup_git()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $SDK_ROOT

	$isprint \
	git init

	$isprint \
	git config user.name $GIT_USER_NAME

	$isprint \
	git config user.email $GIT_USER_EMAIL

	$isprint \
	git add .

	$isprint \
	git commit -m InitialCommit -s

	$isprint \
	ls -al $SDK_ROOT

	display_git_status $isprint

}

#Applying the Patches
apply_patches()
{
	isprint=$1 #Get the Print Command
	PATCH_DIR=$2

	$isprint \
	cd $SDK_ROOT

	if [ -z "$isprint" ]
	then
		msg="About to Apply patches from $PATCH_DIR"
		check_for_confirmation $msg
	fi

	patch_list=$(find $PATCH_DIR/* -type f -name "*.patch" | sort) &&

	for patch in $patch_list; do
		$isprint \
		git am $patch
	done

	$isprint \
	ls -al $SDK_ROOT

	display_git_status $isprint
}

#Build ABOOT
make_aboot()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $SDK_ROOT

	$isprint \
	make aboot ABOOT=1 -j$MAKE_JOB_THREADS

	$isprint \
	ls -al	$SDK_ROOT $OUTPUT_DIR

	display_git_status $isprint
}

#Build Kernel
make_kernel()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $SDK_ROOT

	$isprint \
	make kernel -j$MAKE_JOB_THREADS

	$isprint \
	ls -al	$SDK_ROOT $OUTPUT_DIR

	display_git_status $isprint
}

#Build System
make_system()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $SDK_ROOT

	$isprint \
	make rootfs -j$MAKE_JOB_THREADS

	$isprint \
	ls -al	$SDK_ROOT $OUTPUT_DIR

	display_git_status $isprint
}

#Build Sample Apps
make_sample_apps()
{
	isprint=$1 #Get the Print Command

	$isprint \
	mkdir -p $SAMPLE_BIN_DIR

	$isprint \
	cd $SAMPLE_APPS_DIR

	$isprint \
	make -j$MAKE_JOB_THREADS

	$isprint \
	ls -al	$SDK_ROOT $OUTPUT_DIR $SAMPLE_BIN_DIR

	display_git_status $isprint
}

#Build All Images
build_all()
{
	isprint=$1 #Get the Print Command

	if [ -z "$isprint" ]
	then
		msg="Proceed with Build. Please Confirm"
		check_for_confirmation $msg
	fi

	make_aboot $isprint

	make_kernel $isprint

	make_sample_apps $isprint

	make_system $isprint
}

#Final Set of Instructions and Messages
post_setup()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $WORKDIR

	if [ -z "$isprint" ]
	then
		msg="Build Completed Exit Now"
		check_for_confirmation $msg
	fi

}

#Final Set of Instructions and Messages
exit_step()
{
	cd $WORKDIR
}

#===============================================================================#
#Utility Functions
#===============================================================================#
color()
{
	echo -e $1
}
print_line()
{
	echo -e $RED"========================================================================================================================" $ENDCOLOR
}
print_limits()
{
	intent=$1
	msg=$2
	print_line
	echo -e $YELLOW $@
	print_line
}

wait_for_user()
{
	msg=$@
	color $MAGENTA $UNDER
	echo $msg
	if [ "$ENABLE_PROMTS" = "1" ]
	then
		read -p "Enter to Continue"
	fi
	color $ENDCOLOR
}

exectute_cmd()
{
	call_fn=$1
	display_msg=$2
	print_limits $display_msg Commands
	color $CYAN
	$call_fn "echo" $3
	if [ "$PRINT_ONLY" != "1" ]
	then
		print_line
		wait_for_user "Please verify the instructions and proceed with execution"
		print_limits $display_msg Begin
		$call_fn "" $3
		print_limits $display_msg End
	fi
}

#===============================================================================#
#Color Defines
#===============================================================================#
set_colors()
{
	UNDER='\e[4m'
	RED='\e[31;1m'
	GREEN='\e[32;1m'
	YELLOW='\e[33;1m'
	BLUE='\e[34;1m'
	MAGENTA='\e[35;1m'
	CYAN='\e[36;1m'
	WHITE='\e[37;1m'
	ENDCOLOR='\e[0m'
}
#===============================================================================#
#Build Routine
#===============================================================================#
setup_and_build()
{
	exectute_cmd start_instructions "Instructions"
	exectute_cmd install_tools "Setting Up the Tools Required"
	exectute_cmd setup_root "Setting Up the Root Folder Stucture"
	exectute_cmd setup_tool_chain "Setting Up Tool Chain"
	exectute_cmd setup_sdk "Setting Up SDK"
	exectute_cmd setup_git "Setting Up GIT"
	exectute_cmd apply_patches "Applying the Patches" $PATCH_DIR2
	exectute_cmd build_all "Building All Images"
	exectute_cmd post_setup "Finishing Build Setup"
}

#===============================================================================#
# Update Functions
#===============================================================================#

update_build_name()
{
	local value
	echo "Current Value :" $BUILDNAME
	read -r -p "Enter new Build Name : " value
	BUILDNAME=$value
	echo "New Value :" $BUILDNAME
	_reconfig
}

update_num_jobs()
{
	local value
	echo "Current Value :" $MAKE_JOB_THREADS
	read -r -p "Enter new Num Jobs : " value
	MAKE_JOB_THREADS=$value
	echo "New Value :" $MAKE_JOB_THREADS
	_reconfig
}

update_enable_disable_promts()
{
	local value
	echo "Current Value :" $ENABLE_PROMTS
	read -r -p "Enable/Disable Promts(0/1) : " value
	ENABLE_PROMTS=$value
	echo "New Value :" $ENABLE_PROMTS
	_reconfig
}

update_enable_print_only()
{
	local value
	echo "Current Value :" $PRINT_ONLY
	read -r -p "Enable Print Only (0/1) : " value
	PRINT_ONLY=$value
	echo "New Value :" $PRINT_ONLY
	_reconfig
}

update_no_tool_chain_update()
{
	local value
	echo "Current Value :" $NO_TOOL_CHAIN_UPDATE
	read -r -p "Enable/Disable Tool Chain Update(0/1) : " value
	NO_TOOL_CHAIN_UPDATE=$value
	echo "New Value :" $NO_TOOL_CHAIN_UPDATE
	_reconfig
}

update_git_config_name()
{
	local value
	echo "Current Value :" $GIT_USER_NAME
	read -r -p "Enter Git User Name : " value
	GIT_USER_NAME=$value
	echo "New Value :" $GIT_USER_NAME
	_reconfig
}

update_git_config_email()
{
	local value
	echo "Current Value :" $GIT_USER_EMAIL
	read -r -p "Enter Git User Email : " value
	GIT_USER_EMAIL=$value
	echo "New Value :" $GIT_USER_EMAIL
	_reconfig
}

#===============================================================================#
# Menu Config
#===============================================================================#
show_menu()
{
	display_msg="MAIN MENU"
	print_limits $display_msg
	echo " 1. Build SDK"
	echo " 2. Make ABOOT"
	echo " 3. Make Kernel"
	echo " 4. Make Sample Apps"
	echo " 5. Make ROOTFS"
	echo " 6. Print All Configs"
	echo " 7. Update Build Name"
	echo " 8. Update Num Jobs"
	echo " 9. Enable/Disable Promts"
	echo "10. Enable Print Only"
	echo "11. Enable/Disable Tool Chain Update"
	echo "12. Configure Git User Name"
	echo "13. Configure Git User Email"
	echo "100. Quit"
}

execute_functions()
{
	selection=$1
	echo execute_functions $selection
	case $selection in
		1 ) setup_and_build ;;
		2 ) exectute_cmd make_aboot "Building ABOOT" ;;
		3 ) exectute_cmd make_kernel "Building Kernel" ;;
		4 ) exectute_cmd make_sample_apps "Building Sample Apps" ;;
		5 ) exectute_cmd make_system "Building RootFS" ;;
		6 ) print_config ;;
		7 ) update_build_name ;;
		8 ) update_num_jobs ;;
		9 ) update_enable_disable_promts ;;
		10) update_enable_print_only ;;
		11) update_no_tool_chain_update ;;
		12) update_git_config_name ;;
		13) update_git_config_email ;;
		* ) echo "Incorrect Option Selected" && sleep 1;
	esac

}

read_selection()
{
	local selection
	end_val=$1
	exit_val=$2
	read -r -p "Enter Selection [ 1 - "$end_val"] " selection
	if((($selection<1 || $selection>$end_val) && ($selection!=$exit_val)))
	then
		selection=0
	fi
	return $selection;
}

#===============================================================================#
# Main Function
#===============================================================================#
_main()
{
	_config
	_reconfig
	set_colors
	num_selections=13
	exit_val=100
	while true
	do
		show_menu
		read_selection $num_selections $exit_val
		selection=$?
		if [ $selection -eq $exit_val ]
		then
			echo "Exiting!!" && sleep 2
			break;
		fi
		echo _main execute_functions $selection
		execute_functions $selection
	done
	exit_step
}
_main
