From 1817e707649cb7a82385bf666ba870496198be31 Mon Sep 17 00:00:00 2001
From: Stephanie Buser <sbuser@codeaurora.org>
Date: Tue, 25 May 2021 18:59:58 +0530
Subject: [PATCH 11/15] Adding Generic I2C test Application

---
 sample/i2c2/Makefile      |  26 ++++
 sample/i2c2/example_i2c.c | 333 ++++++++++++++++++++++++++++++++++++++++++++++
 2 files changed, 359 insertions(+)
 create mode 100644 sample/i2c2/Makefile
 create mode 100644 sample/i2c2/example_i2c.c

diff --git a/sample/i2c2/Makefile b/sample/i2c2/Makefile
new file mode 100644
index 0000000..e2c13ed
--- /dev/null
+++ b/sample/i2c2/Makefile
@@ -0,0 +1,26 @@
+CURR_DIR := $(shell pwd)
+
+-include ../../sdk.mk
+
+QL_TARGET_EXE = i2c_test2
+QL_TARGET_OBJS = example_i2c.o
+
+
+CFLAGS := $(QL_SDK_CFLAGS) $(QL_SDK_HARD_CFLAGS)
+LDFLAGS := $(QL_SDK_LDFLAGS) $(QL_SDK_LIBS)
+
+all: $(QL_TARGET_EXE)
+
+%.o:%.c
+	$(CC) -o $@ -c $^ $(CFLAGS)
+
+$(QL_TARGET_EXE): $(QL_TARGET_OBJS)
+	$(CC) -o $@ $(QL_TARGET_OBJS) $(LDFLAGS)
+
+	@echo "Copy "$(QL_TARGET_EXE)" to "$(QL_ROOTFS_BIN_DIR)
+	@cp $(QL_TARGET_EXE) $(QL_ROOTFS_BIN_DIR)
+	@cp $(QL_TARGET_EXE) $(QL_TARGET_OUT_DIR)
+
+clean:
+	rm -rf *.o
+	rm -rf $(QL_TARGET_EXE)
diff --git a/sample/i2c2/example_i2c.c b/sample/i2c2/example_i2c.c
new file mode 100644
index 0000000..aad4e88
--- /dev/null
+++ b/sample/i2c2/example_i2c.c
@@ -0,0 +1,333 @@
+#include <stdio.h>
+#include <stdbool.h>
+#include <stdlib.h>
+#include <string.h>
+#include <time.h>
+#include <inttypes.h>
+#include <sys/types.h>
+#include <sys/stat.h>
+#include <fcntl.h>
+#include <unistd.h>
+#include <errno.h>
+#include <getopt.h>
+#include <sys/ioctl.h>
+#include <linux/i2c.h>
+#include <linux/i2c-dev.h>
+
+#define I2C_DEV         "/dev/i2c-3"
+
+#define DEF_SLAVE_ADDR 	0x18
+#define MAX_I2C_PGM_LEN 1024
+
+static bool verbose;
+static char device_name[] = I2C_DEV;
+static int bus_number = 0;
+static uint8_t slave_address = DEF_SLAVE_ADDR;
+static uint8_t write_buf[MAX_I2C_PGM_LEN] = { 0 };
+static uint8_t read_buf[MAX_I2C_PGM_LEN] = { 0 };
+static int (*call_func)(int) = NULL;
+static int read_len = 0, write_len = 0;
+
+static const struct option lopts[] = {
+	{ "help",      optional_argument, 0, 'h' },
+	{ "busnum",    required_argument, 0, 'b' },
+	{ "slaveaddr", required_argument, 0, 's' },
+	{ "write",     optional_argument, 0, 'w' },
+	{ "read",      optional_argument, 0, 'r' },
+	{ "verbose",   no_argument, 0, 'v' },
+};
+
+static void print_usage(const char *prog)
+{
+	fprintf(stdout, "Usage: %s [-hswrv]\n", prog);
+	puts("  -h --help        display help\n"
+	     "  -s --slaveaddr   i2c slave address\n"
+	     "  -w --write       number of bytes to write for write-then-read if used with -r flag\n"
+	     "                   please specify the data to write like -w <byte1> <byte2> ...\n"
+	     "  -r --read        optional number of bytes to read, use with write flag to do a write followed by a read\n"
+	     "                   default is single byte read.\n"
+	     "  -v --verbose     verbose and prints settings\n");
+
+	     //"  -b --busnum    i2c bus number\n"
+	exit(1);
+}
+
+
+/**
+ * do_rdwr: helper api to read data to the i2c bus and slave using ioctl I2C_RDWR
+ * @param fd The bus file descriptor opened
+ * @param msgs The i2c msg array pointer
+ * @param nmsgs The number of messages in the i2c msg array pointer argument
+ * @return -1 on error and 0 on success
+ */
+static int do_rdwr(int fd, struct i2c_msg *msgs, int nmsgs)
+{
+	struct i2c_rdwr_ioctl_data msgset = {
+		.msgs = msgs,
+		.nmsgs = nmsgs,
+	};
+
+	if (msgs == NULL || nmsgs <= 0)
+		return -1;
+
+	if (ioctl(fd, I2C_RDWR, &msgset) < 0)
+		return -1;
+
+	return 0;
+}
+
+/**
+ * write_i2c_slave: api to write data to the i2c bus and slave
+ * @param fd The bus file descriptor opened
+ * @return -1 on error and 0 on success
+ */
+int write_i2c_slave(int fd)
+{
+	int length;
+
+	struct i2c_msg msgs[] = {
+		[0] = {
+			.addr = slave_address,
+			.flags = 0,
+			.buf = (void *)write_buf,
+			.len = write_len,
+		},
+	};
+
+	if (write_len >= MAX_I2C_PGM_LEN) {
+		return -1;
+	}
+
+	printf("Writing %d bytes to slave-addr: 0x%x offset: "
+						, write_len, slave_address);
+	for (length =0; length < write_len; length++)
+		printf("0x%x ", write_buf[length]);
+
+
+	if (do_rdwr(fd, msgs, 1)) {
+		printf("error: msg transfer failed \n");
+		return -1;
+	}
+	printf("Done \n");
+
+	return 0;
+}
+
+
+/**
+ * print_read_buf: prints the read buffer
+ * @return length of the read buffer
+ */
+static int print_read_buf()
+{
+	int length = read_len;
+	if (!length)
+		return length;
+
+	printf("Data read ");
+	for (length = 0; length <read_len; length++)
+		printf("0x%x ", read_buf[length]);
+	printf("Done\n");
+	return length;
+}
+
+
+/**
+ * read_i2c_slave: does a write followed read i2c operation
+ * @param fd The bus file descriptor opened
+ * @return -1 on error and 0 on success
+ */
+
+static int read_i2c_slave(int fd)
+{
+	struct i2c_msg msgs[] = {
+		[0] = {
+			.addr = slave_address,
+			.flags = 0,
+			.buf = (void *)write_buf,
+			.len = write_len,
+		},
+		[1] = {
+			.addr = slave_address,
+			.flags = I2C_M_RD,
+			.buf = (void *)read_buf,
+			.len = read_len,
+		},
+	};
+
+	printf("Reading %d bytes from slave-addr: 0x%x offset:\n"
+						, read_len,slave_address);
+
+	if (do_rdwr(fd, msgs, 2)) {
+		printf("error: msg transfer failed \n");
+		return -1;
+	}
+	print_read_buf();
+
+	return 0;
+}
+
+
+
+/**
+ * read_i2c_simple: Read from device with a single buffer
+ * @return one on success or negative error code
+ */
+static int read_i2c_simple(int fd)
+{
+	int ret;
+
+	struct i2c_msg msgs[] = {
+		{
+			.addr = slave_address,
+			.flags = I2C_M_RD,
+			.buf = (void *)read_buf,
+			.len = read_len,
+		},
+	};
+	struct i2c_rdwr_ioctl_data msgset = {
+		.msgs = msgs,
+		.nmsgs = 1,
+	};
+
+	printf("Reading: bytes at addr: 0x%x\n",
+					 slave_address);
+	ret = ioctl(fd, I2C_RDWR, &msgset);
+	if (ret < 0) {
+		printf("error: msg transfer failed ret  = %d\n", ret);
+		return -1;
+	} else {
+		print_read_buf();
+		return 0;
+	}
+}
+
+
+/**
+ * parse_opts: parse the options from command line
+ * @return 0 on success.
+ * @param argc standard C argument count
+ * @param argv standard C argument array
+ */
+static int parse_opts(int argc, char *argv[])
+{
+	int c;
+	int i, index = 0;
+
+	while (1) {
+		c = getopt_long(argc, argv, "B:S:W:R:V:b:s:w:r:vh", lopts, NULL);
+		if (c == -1)
+			break;
+
+		switch (c) {
+		case 'B':
+		case 'b':
+			bus_number = strtoul(optarg, NULL, 0);
+			sprintf(device_name,"/dev/i2c-%d",bus_number);
+			break;
+		case 'S':
+		case 's':
+			slave_address = strtoul(optarg, NULL, 0);
+			break;
+		case 'W':
+		case 'w':
+			optind--;
+			while (optind < argc) {
+				if (*argv[optind] != '-') {
+					write_buf[index++] = strtol(argv[optind], NULL, 0);
+					optind++;
+				} else {
+					break;
+				}
+			}
+
+			write_len = index;
+			if (write_len) {
+				if (read_len)
+					call_func = read_i2c_slave;
+				else
+					call_func = write_i2c_slave;
+			} else {
+				printf("-w option requires aleast one argument\n");
+				print_usage(argv[0]);
+				exit(-1);
+			}
+			break;
+		case 'R':
+		case 'r':
+			read_len = strtoul(optarg, NULL, 0);
+
+			if (!read_len) {
+				read_len = 1;
+			}
+			if (write_len)
+				call_func=read_i2c_slave;
+			else
+				call_func=read_i2c_simple;
+			break;
+		case 'V':
+		case 'v':
+			verbose = 1;
+			break;
+		case ':':
+			printf("\nMissing arguments please see help\n");
+		default:
+			print_usage(argv[0]);
+			break;
+		}
+
+	}
+	for (i = optind; i < argc; i++) {
+		printf("\n%s: warning some arguments not parsed : %s",argv[0],argv[i]);
+		break;
+	}
+	return 0;
+}
+
+/**
+ * print_i2c_config: This function is called on verbose prints to print the configuration used
+*/
+static void print_i2c_config(void)
+{
+	int length;
+	printf("\nBus No /dev/i2c-%d",bus_number);
+	printf("\nSlave address 0x%x",slave_address);
+	printf("\nRead length %d",read_len);
+	printf("\nWrite length %d",write_len);
+	printf("\nData for write : ");
+	for (length = 0; length <write_len; length++)
+		printf("0x%x ", write_buf[length]);
+	printf("\n\n");
+}
+
+
+int main(int argc, char *argv[])
+{
+	int fd;
+	int rc = 0;
+
+	if (parse_opts(argc, argv))
+		return 1;
+
+	if (verbose)
+		print_i2c_config();
+
+
+	fd = open(device_name, O_RDWR);
+	if (-1 == fd) {
+		rc = -1;
+		printf("Could not open device %s\n", device_name);
+		goto err_open;
+	}
+	if (call_func) {
+		rc = (*call_func)(fd);
+	} else {
+		printf("No read or write parameter/operation specified\n");
+		close(fd);
+		print_usage(argv[0]);
+	}
+	close(fd);
+
+err_open:
+	return rc;
+}
-- 
2.7.4

